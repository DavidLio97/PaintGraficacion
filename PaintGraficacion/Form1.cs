﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace PaintGraficacion
{
    public partial class paintGraficacion : Form
    {

        Graphics g;
        Pen myPen = new Pen(Color.Black, 3);
        Color colorSeleccionado = Color.Black;
        Color colorFondo = Color.Gray;
        Boolean mouseAbajo;
        Herramientas herramientaSeleccionada = Herramientas.Linea;
        Font fuente = new Font("Arial", 10);
        int tamañoFuente = 10;

        private int cX, cY, x, y, dX, dY;
        Point sp = new Point(0, 0);
        Point ep = new Point(0, 0);



        enum Herramientas
        {
            Linea,
            Rectangulo,
            Circulo,
            ManoAlzada,
            Texto,
            Triangulo,
            Rombo,
            Borrador
        }

        private void paintGraficacion_Load(object sender, EventArgs e)
        {
            g = zonaDibujo.CreateGraphics();
            zonaDibujo.BackColor = colorFondo;
            cbFonts.SelectedIndex = 0;
            cbTamañoFuente.SelectedIndex = 0;
            gbParametrosTexto.Enabled = false;
        }

        public paintGraficacion()
        {
            InitializeComponent();
        }

        //EVENTOS

        private void btnColor_Click(object sender, EventArgs e)
        {
            cambiarColorPluma();
        }

        private void establecerCoordenadasIniciales(object sender, MouseEventArgs e)
        {

            sp = e.Location;
            if (e.Button == MouseButtons.Left)
            {
                mouseAbajo = true;
            }
            cX = e.X;
            cY = e.Y;
        }

        private void zonaDibujo_MouseClick(object sender, MouseEventArgs e)
        {

            if (herramientaSeleccionada == Herramientas.Texto)
            {
                crearTexto();
            }
            if (mouseAbajo)
            {
                establecerCoordenadasFinales(sender, e);
                if (herramientaSeleccionada == Herramientas.Linea) crearLineaRecta(e.X, e.Y);
                if (herramientaSeleccionada == Herramientas.Rectangulo) crearRectangulo();
                if (herramientaSeleccionada == Herramientas.Circulo) crearCirculo();
                if (herramientaSeleccionada == Herramientas.Triangulo) crearTriangulo(e.X,e.Y);
                if (herramientaSeleccionada == Herramientas.Rombo) crearRombo(e.X, e.Y);

            }
        }

        private void zonaDibujo_MouseUp(object sender, MouseEventArgs e)
        {
            mouseAbajo = false;
        }

        private void zonaDibujo_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseAbajo)
            {
                ep = e.Location;
                if (herramientaSeleccionada == Herramientas.ManoAlzada) dibujoManoAlzada();
                if (herramientaSeleccionada == Herramientas.Borrador) borrar();
                sp = ep;
            }
        }

        private void btnTexto_Click(object sender, EventArgs e)
        {
            gbParametrosTexto.Enabled = true;
            herramientaSeleccionada = Herramientas.Texto;
        }

        private void btnLimpiar_Click_1(object sender, EventArgs e)
        {
            limpiarZonaDibujo();
        }

        //Cambiar Herramienta

        private void btnLinea_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.Linea;
            gbParametrosTexto.Enabled = false;
        }

        private void btnRectangulo_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.Rectangulo;
            gbParametrosTexto.Enabled = false;
        }

        private void btnCirculo_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.Circulo;
            gbParametrosTexto.Enabled = false;
        }

        private void btnManoAlzada_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.ManoAlzada;
            gbParametrosTexto.Enabled = false;
        }

        private void btnTriangulo_Click(object sender, EventArgs e)
        {
            gbParametrosTexto.Enabled = false;
            herramientaSeleccionada = Herramientas.Triangulo;
        }

        private void btnRombo_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.Rombo;
            gbParametrosTexto.Enabled = false;
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            herramientaSeleccionada = Herramientas.Borrador;
            gbParametrosTexto.Enabled = false;
        }

        //Cambiar Grosor PEN
        private void btnGrosor3_Click(object sender, EventArgs e)
        {
            myPen.Width = 3;
        }

        private void btnGrosor5_Click(object sender, EventArgs e)
        {
            myPen.Width = 5;
        }

        private void btnGrosor7_Click(object sender, EventArgs e)
        {
            myPen.Width = 7;
        }

        private void btnGrosor10_Click(object sender, EventArgs e)
        {
            myPen.Width = 10;
        }

        //Metodos

        private void establecerCoordenadasFinales(object sender, MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
            dX = e.X - cX;
            dY = e.Y - cY;
        }

        private void cambiarColorPluma()
        {
            if (cdColorSeleccionado.ShowDialog() == DialogResult.OK)
            {
                colorSeleccionado = cdColorSeleccionado.Color;
                btnColor.BackColor = colorSeleccionado;
                myPen.Color = colorSeleccionado;
            }
        }

        private void cambiarFuente()
        {
            if (cbFonts.SelectedIndex == 0) fuente = new Font("Arial", tamañoFuente);
            if (cbFonts.SelectedIndex == 1) fuente = new Font("Segoe UI", tamañoFuente);
            if (cbFonts.SelectedIndex == 2) fuente = new Font("Times New Roman", tamañoFuente);

        }

        private void cambiarTamañoFuente()
        {
            if (cbTamañoFuente.SelectedIndex == 0) tamañoFuente = 10;
            if (cbTamañoFuente.SelectedIndex == 1) tamañoFuente = 15;
            if (cbTamañoFuente.SelectedIndex == 2) tamañoFuente = 20;
            if (cbTamañoFuente.SelectedIndex == 3) tamañoFuente = 25;

        }

        private void crearLineaRecta(int X, int Y)
        {
            g.DrawLine(myPen, cX, cY, X, Y);
        }

        private void crearCirculo()
        {
            g.DrawEllipse(myPen, cX, cY, dX, dY);
        }

        private void crearRectangulo()
        {
            g.DrawRectangle(myPen, cX, cY, dX, dY);
        }

        private void crearTexto()
        {
            if (txbTexto.TextLength > 0)
            {
                cambiarTamañoFuente();
                cambiarFuente();
                SolidBrush Color = new SolidBrush(colorSeleccionado);
                g.DrawString(txbTexto.Text, fuente, Color, sp);
            }
        }

        private void dibujoManoAlzada()
        {
            g.DrawLine(myPen, sp, ep);
        }

        private void crearTriangulo(int X, int Y)
        {

            Point punto1 = new Point(cX, cY);
            Point punto2 = new Point(X, Y);
            int dCxX = X - cX;
            Point punto3 = new Point(cX - dCxX, Y);
            if (punto3.X > 0 && punto3.X < zonaDibujo.Width)
            {
                Point[] points = { punto1, punto2, punto3 };
                g.DrawPolygon(myPen, points);
            }
            
            
        }

        private void crearRombo(int X, int Y)
        {

            Point punto1 = new Point(cX, cY);
            Point punto2 = new Point(X, Y);
            int dCyY = cY - Y;
            int dCxX = X - cX;
            Point punto3 = new Point(cX,Y-dCyY);
            Point punto4 = new Point(cX - dCxX,Y);
            if (punto4.X > 0 && punto4.X < zonaDibujo.Width && punto3.Y > 0 && punto3.Y < zonaDibujo.Height)
            {
                Point[] points = { punto1, punto2,punto3, punto4 };
                g.DrawPolygon(myPen, points);
            }


        }

        private void borrar()
        {
            Pen borrador = new Pen(colorFondo, 20);
            g.DrawLine(borrador, sp, ep);
        }

        private void limpiarZonaDibujo()
        {
            zonaDibujo.Refresh();
            zonaDibujo.BackColor = colorFondo;
        }

        private void btnColorFondo_Click(object sender, EventArgs e)
        {
            if (cdColorSeleccionado.ShowDialog() == DialogResult.OK)
            {
                colorFondo = cdColorSeleccionado.Color;
                btnColorFondo.BackColor = colorFondo;
                zonaDibujo.BackColor = colorFondo;
            }
        }


        //Abrir, Guardar Imagen y Nuevo

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarZonaDibujo();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            guardarImagen();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            abrirImagen();
        }

        private void guardarImagen()
        {
            Bitmap bmp = new Bitmap(zonaDibujo.Width, zonaDibujo.Height);
            Graphics g = Graphics.FromImage(bmp);
            Rectangle rect = zonaDibujo.RectangleToScreen(zonaDibujo.ClientRectangle);
            g.CopyFromScreen(rect.Location, Point.Empty, zonaDibujo.Size);
            g.Dispose();
            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "Png files|*.png|jpeg files|*jpg|bitmaps|*.bmp";
            if (s.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(s.FileName))
                {
                    File.Delete(s.FileName);
                }
                if (s.FileName.Contains(".jpg"))
                {
                    bmp.Save(s.FileName, ImageFormat.Jpeg);
                }
                else if (s.FileName.Contains(".png"))
                {
                    bmp.Save(s.FileName, ImageFormat.Png);
                }
                else if (s.FileName.Contains(".bmp"))
                {
                    bmp.Save(s.FileName, ImageFormat.Bmp);
                }
            }
        }

        private void abrirImagen()
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "Png files|*.png|jpeg files|*jpg|bitmaps|*.bmp";
            if (o.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                zonaDibujo.BackgroundImage = (Image)Image.FromFile(o.FileName).Clone();
            }
        }

    }
}

﻿namespace PaintGraficacion
{
    partial class paintGraficacion
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(paintGraficacion));
            this.zonaDibujo = new System.Windows.Forms.Panel();
            this.btnColor = new System.Windows.Forms.Button();
            this.cdColorSeleccionado = new System.Windows.Forms.ColorDialog();
            this.btnLinea = new System.Windows.Forms.Button();
            this.btnGrosor10 = new System.Windows.Forms.Button();
            this.btnGrosor7 = new System.Windows.Forms.Button();
            this.btnGrosor5 = new System.Windows.Forms.Button();
            this.btnGrosor3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnRombo = new System.Windows.Forms.Button();
            this.btnTriangulo = new System.Windows.Forms.Button();
            this.btnTexto = new System.Windows.Forms.Button();
            this.btnRectangulo = new System.Windows.Forms.Button();
            this.btnCirculo = new System.Windows.Forms.Button();
            this.btnManoAlzada = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.txbTexto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbFonts = new System.Windows.Forms.ComboBox();
            this.cbTamañoFuente = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gbParametrosTexto = new System.Windows.Forms.GroupBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnColorFondo = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.gbParametrosTexto.SuspendLayout();
            this.SuspendLayout();
            // 
            // zonaDibujo
            // 
            this.zonaDibujo.AutoSize = true;
            this.zonaDibujo.BackColor = System.Drawing.Color.Gray;
            this.zonaDibujo.Location = new System.Drawing.Point(124, 12);
            this.zonaDibujo.Name = "zonaDibujo";
            this.zonaDibujo.Size = new System.Drawing.Size(831, 576);
            this.zonaDibujo.TabIndex = 0;
            this.zonaDibujo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.zonaDibujo_MouseClick);
            this.zonaDibujo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.establecerCoordenadasIniciales);
            this.zonaDibujo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.zonaDibujo_MouseMove);
            this.zonaDibujo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.zonaDibujo_MouseUp);
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.Black;
            this.btnColor.Location = new System.Drawing.Point(8, 460);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(113, 26);
            this.btnColor.TabIndex = 0;
            this.btnColor.UseVisualStyleBackColor = false;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnLinea
            // 
            this.btnLinea.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLinea.Location = new System.Drawing.Point(14, 19);
            this.btnLinea.Name = "btnLinea";
            this.btnLinea.Size = new System.Drawing.Size(25, 27);
            this.btnLinea.TabIndex = 0;
            this.btnLinea.Text = "\\";
            this.btnLinea.UseVisualStyleBackColor = true;
            this.btnLinea.Click += new System.EventHandler(this.btnLinea_Click);
            // 
            // btnGrosor10
            // 
            this.btnGrosor10.Image = ((System.Drawing.Image)(resources.GetObject("btnGrosor10.Image")));
            this.btnGrosor10.Location = new System.Drawing.Point(63, 521);
            this.btnGrosor10.Margin = new System.Windows.Forms.Padding(1);
            this.btnGrosor10.Name = "btnGrosor10";
            this.btnGrosor10.Size = new System.Drawing.Size(47, 18);
            this.btnGrosor10.TabIndex = 33;
            this.btnGrosor10.UseVisualStyleBackColor = true;
            this.btnGrosor10.Click += new System.EventHandler(this.btnGrosor10_Click);
            // 
            // btnGrosor7
            // 
            this.btnGrosor7.Image = ((System.Drawing.Image)(resources.GetObject("btnGrosor7.Image")));
            this.btnGrosor7.Location = new System.Drawing.Point(63, 504);
            this.btnGrosor7.Margin = new System.Windows.Forms.Padding(1);
            this.btnGrosor7.Name = "btnGrosor7";
            this.btnGrosor7.Size = new System.Drawing.Size(47, 15);
            this.btnGrosor7.TabIndex = 32;
            this.btnGrosor7.UseVisualStyleBackColor = true;
            this.btnGrosor7.Click += new System.EventHandler(this.btnGrosor7_Click);
            // 
            // btnGrosor5
            // 
            this.btnGrosor5.Image = ((System.Drawing.Image)(resources.GetObject("btnGrosor5.Image")));
            this.btnGrosor5.Location = new System.Drawing.Point(15, 523);
            this.btnGrosor5.Margin = new System.Windows.Forms.Padding(1);
            this.btnGrosor5.Name = "btnGrosor5";
            this.btnGrosor5.Size = new System.Drawing.Size(47, 15);
            this.btnGrosor5.TabIndex = 31;
            this.btnGrosor5.UseVisualStyleBackColor = true;
            this.btnGrosor5.Click += new System.EventHandler(this.btnGrosor5_Click);
            // 
            // btnGrosor3
            // 
            this.btnGrosor3.Image = ((System.Drawing.Image)(resources.GetObject("btnGrosor3.Image")));
            this.btnGrosor3.Location = new System.Drawing.Point(14, 504);
            this.btnGrosor3.Margin = new System.Windows.Forms.Padding(1);
            this.btnGrosor3.Name = "btnGrosor3";
            this.btnGrosor3.Size = new System.Drawing.Size(47, 15);
            this.btnGrosor3.TabIndex = 30;
            this.btnGrosor3.UseVisualStyleBackColor = true;
            this.btnGrosor3.Click += new System.EventHandler(this.btnGrosor7_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 489);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Grosores";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBorrar);
            this.groupBox1.Controls.Add(this.btnRombo);
            this.groupBox1.Controls.Add(this.btnTriangulo);
            this.groupBox1.Controls.Add(this.btnTexto);
            this.groupBox1.Controls.Add(this.btnRectangulo);
            this.groupBox1.Controls.Add(this.btnCirculo);
            this.groupBox1.Controls.Add(this.btnManoAlzada);
            this.groupBox1.Controls.Add(this.btnLinea);
            this.groupBox1.Location = new System.Drawing.Point(8, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(113, 111);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Herramientas";
            // 
            // btnBorrar
            // 
            this.btnBorrar.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.Location = new System.Drawing.Point(45, 80);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(25, 25);
            this.btnBorrar.TabIndex = 43;
            this.btnBorrar.Text = "";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnRombo
            // 
            this.btnRombo.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRombo.Location = new System.Drawing.Point(14, 80);
            this.btnRombo.Name = "btnRombo";
            this.btnRombo.Size = new System.Drawing.Size(25, 25);
            this.btnRombo.TabIndex = 42;
            this.btnRombo.Text = "";
            this.btnRombo.UseVisualStyleBackColor = true;
            this.btnRombo.Click += new System.EventHandler(this.btnRombo_Click);
            // 
            // btnTriangulo
            // 
            this.btnTriangulo.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTriangulo.Location = new System.Drawing.Point(76, 50);
            this.btnTriangulo.Name = "btnTriangulo";
            this.btnTriangulo.Size = new System.Drawing.Size(25, 25);
            this.btnTriangulo.TabIndex = 41;
            this.btnTriangulo.Text = "";
            this.btnTriangulo.UseVisualStyleBackColor = true;
            this.btnTriangulo.Click += new System.EventHandler(this.btnTriangulo_Click);
            // 
            // btnTexto
            // 
            this.btnTexto.Font = new System.Drawing.Font("Segoe MDL2 Assets", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTexto.Location = new System.Drawing.Point(45, 50);
            this.btnTexto.Name = "btnTexto";
            this.btnTexto.Size = new System.Drawing.Size(25, 25);
            this.btnTexto.TabIndex = 40;
            this.btnTexto.Text = "";
            this.btnTexto.UseVisualStyleBackColor = true;
            this.btnTexto.Click += new System.EventHandler(this.btnTexto_Click);
            // 
            // btnRectangulo
            // 
            this.btnRectangulo.Font = new System.Drawing.Font("Segoe MDL2 Assets", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRectangulo.Location = new System.Drawing.Point(45, 19);
            this.btnRectangulo.Name = "btnRectangulo";
            this.btnRectangulo.Size = new System.Drawing.Size(25, 27);
            this.btnRectangulo.TabIndex = 39;
            this.btnRectangulo.Text = "";
            this.btnRectangulo.UseVisualStyleBackColor = true;
            this.btnRectangulo.Click += new System.EventHandler(this.btnRectangulo_Click);
            // 
            // btnCirculo
            // 
            this.btnCirculo.Font = new System.Drawing.Font("Segoe MDL2 Assets", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCirculo.Location = new System.Drawing.Point(76, 19);
            this.btnCirculo.Name = "btnCirculo";
            this.btnCirculo.Size = new System.Drawing.Size(25, 27);
            this.btnCirculo.TabIndex = 38;
            this.btnCirculo.Text = "";
            this.btnCirculo.UseVisualStyleBackColor = true;
            this.btnCirculo.Click += new System.EventHandler(this.btnCirculo_Click);
            // 
            // btnManoAlzada
            // 
            this.btnManoAlzada.Font = new System.Drawing.Font("Segoe MDL2 Assets", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManoAlzada.Location = new System.Drawing.Point(14, 50);
            this.btnManoAlzada.Name = "btnManoAlzada";
            this.btnManoAlzada.Size = new System.Drawing.Size(25, 25);
            this.btnManoAlzada.TabIndex = 37;
            this.btnManoAlzada.Text = "";
            this.btnManoAlzada.UseVisualStyleBackColor = true;
            this.btnManoAlzada.Click += new System.EventHandler(this.btnManoAlzada_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 444);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Color Trazado";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(65, 9);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(25, 23);
            this.btnGuardar.TabIndex = 0;
            this.btnGuardar.Text = "";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbrir.Location = new System.Drawing.Point(34, 9);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(25, 23);
            this.btnAbrir.TabIndex = 35;
            this.btnAbrir.Text = "";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // txbTexto
            // 
            this.txbTexto.Location = new System.Drawing.Point(14, 34);
            this.txbTexto.Name = "txbTexto";
            this.txbTexto.Size = new System.Drawing.Size(87, 20);
            this.txbTexto.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Texto A Escribir";
            // 
            // cbFonts
            // 
            this.cbFonts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFonts.FormattingEnabled = true;
            this.cbFonts.Items.AddRange(new object[] {
            "Arial",
            "Segoe UI",
            "Times New Roman"});
            this.cbFonts.Location = new System.Drawing.Point(14, 83);
            this.cbFonts.Name = "cbFonts";
            this.cbFonts.Size = new System.Drawing.Size(87, 21);
            this.cbFonts.TabIndex = 1;
            // 
            // cbTamañoFuente
            // 
            this.cbTamañoFuente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTamañoFuente.FormattingEnabled = true;
            this.cbTamañoFuente.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "25"});
            this.cbTamañoFuente.Location = new System.Drawing.Point(14, 132);
            this.cbTamañoFuente.Name = "cbTamañoFuente";
            this.cbTamañoFuente.Size = new System.Drawing.Size(87, 21);
            this.cbTamañoFuente.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Fuente";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Tamaño Fuente";
            // 
            // gbParametrosTexto
            // 
            this.gbParametrosTexto.Controls.Add(this.label5);
            this.gbParametrosTexto.Controls.Add(this.cbFonts);
            this.gbParametrosTexto.Controls.Add(this.label4);
            this.gbParametrosTexto.Controls.Add(this.txbTexto);
            this.gbParametrosTexto.Controls.Add(this.cbTamañoFuente);
            this.gbParametrosTexto.Controls.Add(this.label3);
            this.gbParametrosTexto.Location = new System.Drawing.Point(8, 223);
            this.gbParametrosTexto.Name = "gbParametrosTexto";
            this.gbParametrosTexto.Size = new System.Drawing.Size(113, 160);
            this.gbParametrosTexto.TabIndex = 0;
            this.gbParametrosTexto.TabStop = false;
            this.gbParametrosTexto.Text = "Parametros Texto";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(8, 565);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(110, 23);
            this.btnLimpiar.TabIndex = 0;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 399);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Color Fondo";
            // 
            // btnColorFondo
            // 
            this.btnColorFondo.BackColor = System.Drawing.Color.Black;
            this.btnColorFondo.Location = new System.Drawing.Point(8, 415);
            this.btnColorFondo.Name = "btnColorFondo";
            this.btnColorFondo.Size = new System.Drawing.Size(113, 26);
            this.btnColorFondo.TabIndex = 37;
            this.btnColorFondo.UseVisualStyleBackColor = false;
            this.btnColorFondo.Click += new System.EventHandler(this.btnColorFondo_Click);
            // 
            // paintGraficacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 615);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnColorFondo);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnAbrir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGrosor10);
            this.Controls.Add(this.btnGrosor7);
            this.Controls.Add(this.btnGrosor5);
            this.Controls.Add(this.btnGrosor3);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.zonaDibujo);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbParametrosTexto);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "paintGraficacion";
            this.Text = "Paint Graficacion";
            this.Load += new System.EventHandler(this.paintGraficacion_Load);
            this.groupBox1.ResumeLayout(false);
            this.gbParametrosTexto.ResumeLayout(false);
            this.gbParametrosTexto.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel zonaDibujo;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.ColorDialog cdColorSeleccionado;
        private System.Windows.Forms.Button btnLinea;
        private System.Windows.Forms.Button btnGrosor10;
        private System.Windows.Forms.Button btnGrosor7;
        private System.Windows.Forms.Button btnGrosor5;
        private System.Windows.Forms.Button btnGrosor3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.Button btnRectangulo;
        private System.Windows.Forms.Button btnCirculo;
        private System.Windows.Forms.Button btnManoAlzada;
        private System.Windows.Forms.Button btnTexto;
        private System.Windows.Forms.TextBox txbTexto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbFonts;
        private System.Windows.Forms.ComboBox cbTamañoFuente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnTriangulo;
        private System.Windows.Forms.Button btnRombo;
        private System.Windows.Forms.GroupBox gbParametrosTexto;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnColorFondo;
    }
}

